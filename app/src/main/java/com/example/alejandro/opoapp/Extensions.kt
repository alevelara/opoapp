package com.example.alejandro.opoapp

import android.annotation.SuppressLint
import android.content.Context
import android.support.annotation.IntegerRes
import android.support.annotation.StringRes
import android.widget.Toast

fun Context?.showToast(message: String?) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Context?.showToast(@StringRes message: Int) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}


@SuppressLint("ResourceType")
fun Context?.validateFields(@StringRes minutes: String, @StringRes seconds: String, @StringRes interval: String) : Boolean{

    var fieldsValidate : Boolean = true

    if (minutes.isEmpty()){
        showToast(R.string.minuteMessage)
        return false
    }

    if (minutes.toInt() < 0){
        showToast(R.string.minMinutesValue)
        return false
    }

    if (minutes.toInt() >= 60 ){
        showToast(R.string.maxMinutesValue)
        return false
    }

    if (seconds.isEmpty()){
        showToast(R.string.secondMessage)
        return false
    }

    if (seconds.toInt() < 0){
        showToast(R.string.minSecondsValue)
        return false
    }

    if (seconds.toInt() > 60 ){
        showToast(R.string.maxSecondsValue)
        return false
    }

    if (interval.isEmpty()){
        showToast(R.string.intervalMessage)
        return false
    }

    if (interval.toInt() < 0){
        showToast(R.string.minIntervalValue)
        return false
    }

    if (minutes.toInt() < 0){
        showToast(R.string.minMinutesValue)
        return false
    }

    if (minutes.toInt() >= 60 ){
        showToast(R.string.maxMinutesValue)
        return false
    }

    return fieldsValidate
}


fun Double.format(digits: Int) = String.format("%.${digits}f", this)