package com.example.alejandro.opoapp

import android.content.res.ColorStateList
import android.graphics.Color
import android.media.MediaPlayer
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    private lateinit var mediaPlayer : MediaPlayer
    private lateinit var calculator: Calculator
    private var mStop : Boolean = false
    private var mRun : Boolean = true
    private var minutes: Int = 0
    private var seconds: Int = 0
    private var intervals: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun run(view: View){
        if (validateFields(txtMinutes.text.toString(), txtSeconds.text.toString(), txtInterval.text.toString())){
            evaluateButton()
        }
    }

    fun startSound(mediaPlayer: MediaPlayer){
        mediaPlayer.start()
    }

    fun stopSound(mediaPlayer: MediaPlayer){
        mediaPlayer.stop()
    }

    fun pauseSound(mediaPlayer: MediaPlayer){
        mediaPlayer.pause()
    }

    private fun assignFields(){

        minutes = txtMinutes.text.toString().toInt()
        seconds = txtSeconds.text.toString().toInt()
        intervals = txtInterval.text.toString().toInt()

        calculator = Calculator(minutes , seconds , intervals )

        txtDistanceMpS.text = calculator.getSpeedMeterPerSeconds().format(2).toString() + " m/s"
        txtDistanceKpH.text = calculator.getSpeedKilometersPerHours().format(2).toString() + " Km/h"
        txtTraveledDistance.text = calculator.getDistanceInSingleInterval().format(2).toString() + " Intervals"

    }

    private fun resetFields(){

        txtDistanceMpS.setText( R.string.distanceMpS)
        txtDistanceKpH.setText(R.string.distanceKpH)
        txtTraveledDistance.setText(R.string.traveledDistance)
    }

    private fun changeButtonRunToStop() {
        activateStopMode()
        btnRun.setText(R.string.stop)
        btnRun.setBackgroundColor(Color.WHITE)
        btnRun.backgroundTintList = ColorStateList.valueOf(getColor(R.color.red))
        disableCalculateFields()
    }

    private fun changeButtonStopToRun() {
        activateRunMode()
        btnRun.setText(R.string.run);
        btnRun.setBackgroundColor(Color.WHITE)
        btnRun.backgroundTintList = ColorStateList.valueOf(getColor(R.color.primary))
        enableCalculateFields()
    }

    private fun activateStopMode(){
        mStop = true
        mRun = false
    }

    private fun activateRunMode(){
        mStop = false
        mRun = true
    }

    private fun evaluateButton(){
        if(mRun){
            changeButtonRunToStop()
            assignFields()
        }else{
            changeButtonStopToRun()
            resetFields()
        }

    }

    private fun enableCalculateFields(){
        txtMinutes.isEnabled = true
        txtSeconds.isEnabled = true
        txtInterval.isEnabled = true
    }

    private fun disableCalculateFields(){
        txtMinutes.isEnabled = false
        txtSeconds.isEnabled = false
        txtInterval.isEnabled = false
    }

}
