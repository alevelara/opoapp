package com.example.alejandro.opoapp

import java.lang.Exception
import kotlin.properties.Delegates

/**
 * Created by Alejandro on 10/03/2018.
 */
class Calculator constructor(minutes: Int, seconds: Int, intervals: Int){
    companion object {
        const val METRES : Int = 1000
        const val DISTANCE_KILOMETRES : Int = 1000
        const val MINUTES_IN_A_HOUR : Int = 60
        const val SECONDS_IN_A_MINUTE : Int = 60
        const val SECONDS_IN_A_HOUR : Int = 3600
    }

    var seconds : Int = seconds
        get() = field

    var minutes: Int = minutes
        get() = field

    var intervals : Int = intervals
        get() = field
        set(value) {
            field = value
        }

    private var speedMetrePerSeconds : Double by Delegates.notNull()
    private var speedKilometrePerHours : Double by Delegates.notNull()
    private var intervalsInAKilometer : Double by Delegates.notNull()

    private fun calculateSpeedMetresPerSeconds() : Double {
        try {
            this.speedMetrePerSeconds = METRES.div(calculateSeconds())
        } catch (e : Exception){
            this.speedMetrePerSeconds = 0.0
        }
        return this.speedMetrePerSeconds
    }

    private fun calculateSeconds(): Double {
        return ((minutes * MINUTES_IN_A_HOUR) + seconds).toDouble()
    }

    private fun calculateDecimalSeconds() : Double{
        return calculateSeconds() * 100
    }

    private fun calculateNumberOfIntervals(): Int {
        return DISTANCE_KILOMETRES / intervals
    }

    private fun calculateSpeedKilometersPerHours() : Double{
        try {
            var totalSeconds : Double = calculateSeconds()
            return SECONDS_IN_A_HOUR.div(totalSeconds)

        }catch (e: Exception){
            this.speedKilometrePerHours = 0.0
        }

        return this.speedKilometrePerHours
    }

    fun getSpeedMeterPerSeconds() : Double{
        return calculateSpeedMetresPerSeconds()
    }

    fun getSpeedKilometersPerHours() : Double{
        return calculateSpeedKilometersPerHours()
    }


    fun getDistanceInSingleInterval() : Double {
        try {
            var numIntervals : Int = calculateNumberOfIntervals()
            this.intervalsInAKilometer = numIntervals.toDouble()//(DISTANCE_KILOMETRES / numIntervals).toDouble()
        }catch (e: Exception){
            this.intervalsInAKilometer = 0.0
        }
        return this.intervalsInAKilometer
    }

    override fun toString(): String {
        return "$minutes.toString():$seconds.toString()"
    }

}